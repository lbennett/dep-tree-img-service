// @flow

import AsciiTree from 'ascii-tree';
import Text2png from 'text2png';

class TreeRenderer {
  textColor: string;
  bgColor: string;
  font: string;
  padding: number;

  constructor(options: Object = {}) {
    this.textColor = options.textColor || '#EBEBEB';
    this.bgColor = options.bgColor || '#595959';
    this.font = options.font || '14px Arial';
    this.padding = options.padding || 20;
  }

  parseAndRender(parsable: Object): Promise<Object> {
    return this.parse(parsable).then(this.render.bind(this));
  }

  parse(treeObject: Object): Promise<string> {
    return Promise.resolve('#root\n##node\n###leaf');
  }

  render(treeString: string): { treeString: string, treeASCII: string, treePNG: Buffer } {
    const treeASCII: string = TreeRenderer.renderASCII(treeString);
    const treePNG: Buffer = this.renderTreePNG(treeASCII);

    return {
      treeString,
      treeASCII,
      treePNG,
    };
  }

  renderTreePNG(treeASCII: string): Buffer {
    const treePNG: Buffer = Text2png(treeASCII, {
      font: this.font,
      textColor: this.textColor,
      bgColor: this.bgColor,
      padding: this.padding,
    });

    return treePNG;
  }

  static renderASCII(treeString: string): string {
    const treeASCII: string = AsciiTree.generate(treeString);

    return treeASCII;
  }
}

export default TreeRenderer;
