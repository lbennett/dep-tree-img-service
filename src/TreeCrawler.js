// @flow

import GitLabClient from './GitLabClient';

class TreeCrawler {
  mergeRequests: Object[];

  constructor(mergeRequests: Object[]) {
    this.mergeRequests = mergeRequests;
  }
}

export default TreeCrawler;
