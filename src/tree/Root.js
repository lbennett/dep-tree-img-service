// @flow

import Node from './Node';

class Root {
  rootNode: Node;

  constructor(data: mixed) {
    this.rootNode = new Node(data);
  }
}

export default Root;
