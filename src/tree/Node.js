// @flow

class Node {
  data: mixed;
  parent: ?Object;
  children: Object[];

  constructor(data: mixed) {
    this.data = data;
    this.parent = null;
    this.children = [];
  }
}

export default Node;
