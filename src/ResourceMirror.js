// @flow

import GitLabClient from './GitLabClient';

class ResourceMirror {
  namespaceName: string;
  projectName: string;
  client: GitLabClient;

  constructor(namespaceName: string, projectName: string, options: Object = {}) {
    this.namespaceName = namespaceName;
    this.projectName = projectName;

    this.client = new GitLabClient(options);
  }

  getAllMergeRequests(): Promise<Object[]> {
    return this.client.getProjectID(this.namespaceName, this.projectName)
      .then((projectID) => {
        new Promise((resolve, reject) => {
          this.paginateAllMergeRequests(projectID, 1, resolve, reject);
        });
      });
  }

  paginateAllMergeRequests(projectID: string, page: number, resolve: Function, reject: Function) {
    this.client.getMergeRequests(projectID, {
      page,
      per_page: 100
    }).then(function (mergeRequests, completeMirror) {
      if (completeMirror) return resolve();
      page += 1;
      this.paginateAllMergeRequests(projectID, page, resolve, reject);
    }).catch(reject);
  }
}
