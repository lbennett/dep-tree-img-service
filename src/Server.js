// @flow

import express from 'express';
import morgan from 'morgan';
import Manager from './Manager';

class Server {
  app: Object;
  port: number;

  constructor(options: Object = {}) {
    this.app = express();
    this.port = options.port || 4000;
  }

  init() {
    this.initMiddleware();
    this.initServer();
  }

  initMiddleware() {
    this.app.use(morgan('combined'));
  }

  initServer() {
    this.initEndpoints();

    this.app.listen(this.port, this.running.bind(this));
  }

  initEndpoints() {
    this.app.get('/', this.status.bind(this));

    this.app.get('/:namespaceName/:projectName/:mergeRequestID', (req, res) => {
      Manager.waitForMergeRequests(req, res).then(Manager.serveTree);
    });
  }

  running() {
    /* eslint-disable no-console */
    console.log(`\nrunning on ${this.port}\n`);
    /* eslint-enable no-console */
  }

  status(req: Object, res: Object) {
    res.json({
      status: 'running',
      port: this.port,
    });
  }

  static renderError(res: Object, error: Object) {
    res.json({
      status: error.statusCode,
      message: error.error.message || error.error.error,
    });
  }
}

export default Server;
