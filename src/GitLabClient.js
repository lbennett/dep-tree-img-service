// @flow

import path from 'path';
import url from 'url';
import Request from 'request-promise';

class GitLabClient {
  url: string;
  token: string;
  APIversion: number;
  baseURL: string;
  endpoints: Object;

  constructor(options: Object = {}) {
    this.APIversion = options.APIversion || 4;
    this.token = options.token || 'Z7B9PfajssmygwFNfxP-';

    this.buildEndpointStrings(options);
  }

  buildEndpointStrings(options: Object = {}) {
    const baseURLObject: Object = url.parse(options.host || 'http://localhost:3000');
    baseURLObject.pathname = path.join('api', `v${this.APIversion}`);

    const baseURL: string = url.format(baseURLObject);

    this.endpoints = {
      mergeRequest: `${baseURL}/${path.join('projects', ':projectID', 'merge_requests', ':mergeRequestID')}`,
      mergeRequests: `${baseURL}/${path.join('projects', ':projectID', 'merge_requests')}`,
      projects: `${baseURL}/${path.join('projects', ':namespaceName', ':projectName')}`,
    };
  }

  getMergeRequest(projectID: string, mergeRequestID: string, queryString: Object): Promise<Object> {
    const options: Object = this.getOptions('mergeRequest', {
      projectID,
      mergeRequestID,
    }, queryString);

    return Request(options);
  }

  getMergeRequests(projectID: string, queryString: Object): Promise<Object[]> {
    const options: Object = this.getOptions('mergeRequests', {
      projectID,
    }, queryString);

    return Request(options);
  }

  getProjectID(namespaceName: string, projectName: string): Promise<string> {
    const options: Object = this.getOptions('projects', {
      namespaceName,
      projectName,
    });

    return Request(options).then(console.log).catch(console.log);
  }

  getOptions(endpointKey: string, endpointData: Object, qs: ?Object): Object {
    const uri = this.renderEndpointString(endpointKey, endpointData);

    const options = {
      uri,
      qs,
      json: true,
      headers: {
        'PRIVATE-TOKEN': this.token,
      },
    };

    return options;
  }

  renderEndpointString(endpointKey: string, endpointData: Object): string {
    let endpoint = this.endpoints[endpointKey];

    Object.keys(endpointData).forEach((key) => {
      endpoint = endpoint.replace(`:${key}`, endpointData[key]);
    });

    return endpoint;
  }
}

export default GitLabClient;
