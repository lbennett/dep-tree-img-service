// @flow

import Server from './Server';
import TreeCrawler from './TreeCrawler';
import TreeRenderer from './TreeRenderer';
import ResourceMirror from './ResourceMirror';

class Manager {
  static waitForMergeRequests(req: Object, res: Object): Promise<Object[]> {
    const namespaceName: string = req.params.namespaceName;
    const projectName: string = req.params.projectName;

    const resourceMirror = new ResourceMirror(namespaceName, projectName);

    return resourceMirror.getAllMergeRequests();
  }

  static serveTree(mergeRequests: Object[]): Promise<Object> {
    const treeCrawler: TreeCrawler = new TreeCrawler(mergeRequests);
    const treeRenderer: TreeRenderer = new TreeRenderer();
  }
}

export default Manager;
