'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Server = require('./Server');

var _Server2 = _interopRequireDefault(_Server);

var _TreeCrawler = require('./TreeCrawler');

var _TreeCrawler2 = _interopRequireDefault(_TreeCrawler);

var _TreeRenderer = require('./TreeRenderer');

var _TreeRenderer2 = _interopRequireDefault(_TreeRenderer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Manager = function () {
  function Manager() {
    _classCallCheck(this, Manager);
  }

  _createClass(Manager, null, [{
    key: 'serveTree',
    value: function serveTree(req, res) {
      var namespaceName = req.params.namespaceName;
      var projectName = req.params.projectName;
      var mergeRequestID = req.params.mergeRequestID;

      var treeCrawler = new _TreeCrawler2.default(namespaceName, projectName, mergeRequestID);
      var treeRenderer = new _TreeRenderer2.default();

      return treeCrawler.crawl().then(treeRenderer.parseAndRender.bind(treeRenderer)).then(function (data) {
        return res.end(data.treePNG);
      }).catch(function (err) {
        return _Server2.default.renderError(res, err);
      });
    }
  }]);

  return Manager;
}();

exports.default = Manager;