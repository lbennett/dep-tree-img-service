'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _asciiTree = require('ascii-tree');

var _asciiTree2 = _interopRequireDefault(_asciiTree);

var _text2png = require('text2png');

var _text2png2 = _interopRequireDefault(_text2png);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TreeRenderer = function () {
  function TreeRenderer() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, TreeRenderer);

    this.textColor = options.textColor || '#EBEBEB';
    this.bgColor = options.bgColor || '#595959';
    this.font = options.font || '14px Arial';
    this.padding = options.padding || 20;
  }

  _createClass(TreeRenderer, [{
    key: 'parseAndRender',
    value: function parseAndRender(parsable) {
      return this.parse(parsable).then(this.render.bind(this));
    }
  }, {
    key: 'parse',
    value: function parse(treeObject) {
      return Promise.resolve('#root\n##node\n###leaf');
    }
  }, {
    key: 'render',
    value: function render(treeString) {
      var treeASCII = TreeRenderer.renderASCII(treeString);
      var treePNG = this.renderTreePNG(treeASCII);

      return {
        treeString: treeString,
        treeASCII: treeASCII,
        treePNG: treePNG
      };
    }
  }, {
    key: 'renderTreePNG',
    value: function renderTreePNG(treeASCII) {
      var treePNG = (0, _text2png2.default)(treeASCII, {
        font: this.font,
        textColor: this.textColor,
        bgColor: this.bgColor,
        padding: this.padding
      });

      return treePNG;
    }
  }], [{
    key: 'renderASCII',
    value: function renderASCII(treeString) {
      var treeASCII = _asciiTree2.default.generate(treeString);

      return treeASCII;
    }
  }]);

  return TreeRenderer;
}();

exports.default = TreeRenderer;