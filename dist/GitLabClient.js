'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GitLabClient = function () {
  function GitLabClient() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, GitLabClient);

    this.APIversion = options.APIversion || 4;
    this.token = options.token || 'Z7B9PfajssmygwFNfxP-';

    this.buildEndpointStrings(options);
  }

  _createClass(GitLabClient, [{
    key: 'buildEndpointStrings',
    value: function buildEndpointStrings() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var baseURLObject = _url2.default.parse(options.host || 'http://localhost:3000');
      baseURLObject.pathname = _path2.default.join('api', 'v' + this.APIversion);

      var baseURL = _url2.default.format(baseURLObject);

      this.endpoints = {
        mergeRequests: baseURL + '/' + _path2.default.join('projects', ':projectID', 'merge_requests', ':mergeRequestID'),
        projects: baseURL + '/' + _path2.default.join('projects', ':namespaceName', ':projectName')
      };
    }
  }, {
    key: 'getMergeRequest',
    value: function getMergeRequest(projectID, mergeRequestID) {
      var options = this.getOptions({
        projectID: projectID,
        mergeRequestID: mergeRequestID,
        endpoint: 'mergeRequests'
      });

      return (0, _requestPromise2.default)(options);
    }
  }, {
    key: 'getProjectID',
    value: function getProjectID(namespaceName, projectName) {
      var options = this.getOptions({
        namespaceName: namespaceName,
        projectName: projectName,
        endpoint: 'projects'
      });

      return (0, _requestPromise2.default)(options).then(console.log).catch(console.log);
    }
  }, {
    key: 'getOptions',
    value: function getOptions(data, qs) {
      var uri = this.renderEndpointString(data);

      var options = {
        uri: uri,
        qs: qs,
        json: true,
        headers: {
          'PRIVATE-TOKEN': this.token
        }
      };

      return options;
    }
  }, {
    key: 'renderEndpointString',
    value: function renderEndpointString(data) {
      var endpoint = this.endpoints[data.endpoint];

      Object.keys(data).forEach(function (key) {
        endpoint = endpoint.replace(':' + key, data[key]);
      });

      return endpoint;
    }
  }]);

  return GitLabClient;
}();

exports.default = GitLabClient;