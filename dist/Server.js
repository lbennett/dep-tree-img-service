'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _Manager = require('./Manager');

var _Manager2 = _interopRequireDefault(_Manager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Server = function () {
  function Server() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Server);

    this.app = (0, _express2.default)();
    this.port = options.port || 4000;
  }

  _createClass(Server, [{
    key: 'init',
    value: function init() {
      this.initMiddleware();
      this.initServer();
    }
  }, {
    key: 'initMiddleware',
    value: function initMiddleware() {
      this.app.use((0, _morgan2.default)('combined'));
    }
  }, {
    key: 'initServer',
    value: function initServer() {
      this.initEndpoints();

      this.app.listen(this.port, this.running.bind(this));
    }
  }, {
    key: 'initEndpoints',
    value: function initEndpoints() {
      this.app.get('/', this.status.bind(this));
      this.app.get('/:namespaceName/:projectName/:mergeRequestID', _Manager2.default.serveTree);
    }
  }, {
    key: 'running',
    value: function running() {
      /* eslint-disable no-console */
      console.log('\nrunning on ' + this.port + '\n');
      /* eslint-enable no-console */
    }
  }, {
    key: 'status',
    value: function status(req, res) {
      res.json({
        status: 'running',
        port: this.port
      });
    }
  }], [{
    key: 'renderError',
    value: function renderError(res, error) {
      res.json({
        status: error.statusCode,
        message: error.error.message || error.error.error
      });
    }
  }]);

  return Server;
}();

exports.default = Server;