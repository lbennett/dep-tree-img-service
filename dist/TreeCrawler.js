'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _GitLabClient = require('./GitLabClient');

var _GitLabClient2 = _interopRequireDefault(_GitLabClient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TreeCrawler = function () {
  function TreeCrawler(namespaceName, projectName, mergeRequestID) {
    var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

    _classCallCheck(this, TreeCrawler);

    this.namespaceName = namespaceName;
    this.projectName = projectName;
    this.mergeRequestID = mergeRequestID;

    this.client = new _GitLabClient2.default(options);
  }

  _createClass(TreeCrawler, [{
    key: 'crawl',
    value: function crawl() {
      return this.getStartingMergeRequest().then(TreeCrawler.crawlToExtremities).then(TreeCrawler.combineTrees);
    }
  }, {
    key: 'getStartingMergeRequest',
    value: function getStartingMergeRequest() {
      var _this = this;

      return this.client.getProjectID(this.namespaceName, this.projectName).then(function (projectID) {
        return _this.client.getMergeRequest(projectID, _this.mergeRequestID);
      });
    }
  }], [{
    key: 'crawlToExtremities',
    value: function crawlToExtremities(startingMergeRequest) {
      return Promise.all([TreeCrawler.crawlToRoot(startingMergeRequest), TreeCrawler.crawlToLeafs(startingMergeRequest)]);
    }
  }, {
    key: 'crawlToRoot',
    value: function crawlToRoot(startingMergeRequest) {
      return startingMergeRequest;
    }
  }, {
    key: 'crawlToLeafs',
    value: function crawlToLeafs(startingMergeRequest) {
      return startingMergeRequest;
    }
  }, {
    key: 'combineTrees',
    value: function combineTrees(mergeRequestsTree) {
      return mergeRequestsTree[0];
    }
  }]);

  return TreeCrawler;
}();

exports.default = TreeCrawler;